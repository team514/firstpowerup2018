package frc.robot.subsystems;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 */
public class AutoUtil extends Subsystem {
	AutoMode am;
	StartLocation sl;
	boolean autoDefaultDone, autoDriveDone, autoScoreDone, bypassScore; 
	String gd;
	public NetworkTable camera;
	double d;
	
	public AutoUtil(){
		camera = NetworkTableInstance.getDefault().getTable("limelight");
	}

	public boolean bypassScore(){
		if((gd.equalsIgnoreCase("RRR") && sl.toString().equalsIgnoreCase("Left")) ||
			(gd.equalsIgnoreCase("LLL") && sl.toString().equalsIgnoreCase("Right"))){
			bypassScore = true;
		}else{
			bypassScore = false;
		}
		return bypassScore;
	}
	
	public void setAutoMode(AutoMode am){
		this.am = am;
	}
	
	public void setStartLocation(StartLocation sl){
		this.sl = sl;
	}
	
	public AutoMode getAutoMode(){
		return this.am;
	}
	
	public StartLocation getStartLocation(){
		return this.sl;
	}

	public void setAutoDefaultDone(boolean d) {
		this.autoDefaultDone = d;
	}
	
	public boolean getAutoDefaultDone() {
		return this.autoDefaultDone;
	}
	
	public void setAutoScoreDone(boolean d) {
		this.autoScoreDone = d;
	}
	
	public boolean getAutoScoreDone() {
		return this.autoScoreDone;
	}
	
	public void setAutoDriveDone(boolean d) {
		this.autoDriveDone = d;
	}
	
	public boolean getAutoDriveDone() {
		return this.autoDriveDone;
	}
	
	public void setGameData(String gd) {
		this.gd = gd;
	}
	
	public char getNearSwitch() {
		return this.gd.charAt(0);
	}
	
	public char getScale() {
		return this.gd.charAt(1);
	}
	
	public char getFarSwitch() {
		return this.gd.charAt(2);
	}
	
	public double getHorizontalOffset(){
		NetworkTableEntry tx = camera.getEntry("tx");
		return tx.getDouble(0);
	}
	
	public double getVerticalOffset(){
		//This is not really  part of this years game!
		NetworkTableEntry ty = camera.getEntry("ty");
		return ty.getDouble(0);
	}
	
	public double getTargetArea(){
		NetworkTableEntry ta = camera.getEntry("ta");
		return ta.getDouble(0);
	}
	
	public double calcDistance() {
		this.d = 0.0;
		this.d = (5.9957 * (Math.pow(Math.E,  (-0.04*getTargetArea()))));
		return this.d;
	}
	
	public String getStringGameData() {
		return this.gd;
	}
    // Put methods for controlling this subsystem
    // here. Call these from Commands.

    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
    
    public void updateStatus(){
    	
    	//smartdashboard.put[whatever]
    	//SmartDashboard.putNumber("Horizontal Offset: ", getHorizontalOffset());
		//SmartDashboard.putNumber("Vertical Offset: ", getVerticalOffset());
		//SmartDashboard.putNumber("Target Area: ", getTargetArea());
		//SmartDashboard.putNumber("Distance Calculated: ", calcDistance());
		
    	if(this.am != null){
    		SmartDashboard.putString("AutoMode = ", this.am.toString());
    	}
    	
    	if(this.sl != null){
    		SmartDashboard.putString("StartLocation = ", this.sl.toString());
    	}
    	
    	if(this.gd != null){
    		SmartDashboard.putString("GameData = ", getStringGameData());
    	}
    }
    
    public enum AutoMode{
    	Default,
    	ScoreSwitch,
    	ScoreScale,
    	NoScore,
    	Fun,
    	Test;
    }
    public enum StartLocation{
    	
    	Auto,
    	Left, 
    	Center,
    	Right;
    }
    
    public enum GoalLocation{
    	
    	Close,
    	Scale,
    	Far;
    }
}


